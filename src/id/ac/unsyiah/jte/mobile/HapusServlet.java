package id.ac.unsyiah.jte.mobile;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterPredicate;

@SuppressWarnings("serial")
public class HapusServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException  
	{
		//Ambil ID dari data yang mau ditampilkan
		long keyId = Long.valueOf(req.getParameter("id"));
		Key key = KeyFactory.createKey("DataMahasiswa", keyId);
		
		//Bangun Query
		FilterPredicate filter = new Query.FilterPredicate(Entity.KEY_RESERVED_PROPERTY
															, Query.FilterOperator.EQUAL
															, key);
		Query query = new Query("DataMahasiswa");
		query.setFilter(filter);
		
		//Baca
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		Entity data = preparedQuery.asSingleEntity();
		
		//Kirim ke halaman
		req.setAttribute("data", data);
		
		//Tampilkan hala
		resp.setContentType("text/plain");
		RequestDispatcher jsp = req.getRequestDispatcher("hapus.jsp");
		jsp.forward(req, resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException 
	{
		
				//Ambil ID dari data yang mau ditampilkan
				long keyId = Long.valueOf(req.getParameter("hdnId"));
				Key key = KeyFactory.createKey("DataMahasiswa", keyId);
				
				//hapus
				DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
				datastoreService.delete(key);
		
		resp.sendRedirect("/");
	}
}
