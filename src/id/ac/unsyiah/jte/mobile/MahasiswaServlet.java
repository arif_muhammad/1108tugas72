package id.ac.unsyiah.jte.mobile;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class MahasiswaServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException 
	{
		Query query = new Query("DataMahasiswa");
		
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		
		FetchOptions fetchOptions = FetchOptions.Builder.withOffset(0);
		List<Entity> daftarNama  = preparedQuery.asList(fetchOptions);
		
		req.setAttribute("daftarNama", daftarNama);
		
		resp.setContentType("text/plain");
		RequestDispatcher jsp = req.getRequestDispatcher("indexMahasiswa.jsp");
		jsp.forward(req, resp);
	}
}
