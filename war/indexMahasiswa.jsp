<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<style>
table,th,td
	{
		border:1px solid black;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Database</title>
</head>

<body>
	<h1>Data yang Tersimpan</h1>

	<a href="/tambah">Tambah</a></br>

	<table style="width:500px">

    	<tr>
        	<th>NAMA</th>
        	<th>NIM</th>
        	<th>Email</th>
        	<th>NoHP</th>
        	<th>Aktif</th>
        	<th>&nbsp;</th>
    	</tr>
 	<c:forEach var="satu" items="${daftarNama}">
    	<tr>
			<td align="center">
				<a href="/ubah?id=${satu.key.id}">${satu.properties.nama}</a><br/>
        	</td>
        	<td align="justify">
				<a href="/ubah?id=${satu.key.id}">${satu.properties.nim}</a></br>															
			</td>
			<td align="justify">
				<a href="/ubah?id=${satu.key.id}">${satu.properties.email}</a></br>															
			</td>
			<td align="justify">
				<a href="/ubah?id=${satu.key.id}">${satu.properties.noHP}</a></br>															
			</td>
			<td align="justify">
				<a href="/ubah?id=${satu.key.id}">${satu.properties.aktif}</a></br>															
			</td>
			<td>
				<a href="/hapus?id=${satu.key.id}">Hapus</a><br/>
			</td>
    	</tr>
	</c:forEach>
	</table>
</body>
</html>